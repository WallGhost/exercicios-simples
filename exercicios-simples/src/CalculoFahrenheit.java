import java.util.Scanner;

public class CalculoFahrenheit {
	
	//M�todo que faz o c�lculo de Fahrenheit para Celsius
	public static double calculaFahrenheit(double grau) {
		double tempFahrenheit;
		
		tempFahrenheit = (grau * 9/5) + 32;
		
		return tempFahrenheit;
	}
	
	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		
		//Declara��o de Vari�veis
		double grauCelsius, result;
		
		System.out.println("Digite o valor do grau que gostaria de calcular: ");
		grauCelsius = teclado.nextDouble();
		
		result = calculaFahrenheit(grauCelsius);
		
		System.out.println("Grau em em Celsius: " + grauCelsius);
		System.out.println("Grau em Fahrenheit: " + result);
	}

}
