import java.util.Scanner;

public class AreaCirculo {
	
	//M�todo que faz o c�lculo da �rea do c�rculo mencionado pelo usu�rio
	public static double calculaArea(double raio) {
		
		final double pi = 3.14; //Declarando a constante de PI
		double result;
		
		return result = pi *(raio * raio); //Formula do c�lculo da �rea
	}
	
	public static void main(String[] args) {
		
		//Declara��o de vari�veis
		double raio,area;
		Scanner teclado = new Scanner(System.in);
		
		//Come�o do programa
		System.out.println("Escreva o raio do c�rculo: ");
		raio = teclado.nextDouble();
		
		area = calculaArea(raio);
		
		System.out.println("Valor passado do raio: " + raio);
		System.out.println("O raio da circunfer�ncia mencionada �: " + area);
		
	}
	

}
