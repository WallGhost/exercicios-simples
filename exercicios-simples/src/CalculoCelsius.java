import java.util.Scanner;

public class CalculoCelsius {
	
	//M�todo que faz o c�lculo de Fahrenheit para Celsius
	public static double calculaCelsius(double grau) {
		double tempCelsius;
		
		tempCelsius = ((grau - 32) * 5) / 9;
		
		return tempCelsius;
	}
	
	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		
		//Declara��o de Vari�veis
		double grauFahrenheit, result;
		
		System.out.println("Digite o valor do grau que gostaria de calcular: ");
		grauFahrenheit = teclado.nextDouble();
		
		result = calculaCelsius(grauFahrenheit);
		
		System.out.println("Grau em Fahrenheit: " + grauFahrenheit);
		System.out.println("Grau em em Celsius: " + result);
	}

}
