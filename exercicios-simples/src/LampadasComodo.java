import java.util.Scanner;

public class LampadasComodo {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		
		//Declara��o das vari�veis
		int lampadas;
		float comprimento, largura,areaComodo, potTotal, watts, result;
		
		System.out.println("Digite a pot�ncia da l�mpada � ser usada: ");
		watts = teclado.nextFloat();
		
		System.out.println("Digite o comprimento do c�modo: ");
		comprimento = teclado.nextFloat();
		System.out.println("Digite a largura do c�modo: ");
		largura = teclado.nextFloat();
		
		areaComodo = comprimento * largura;
		potTotal = areaComodo * 18;
		result = potTotal / watts;
		lampadas = Math.round(result);
		
		System.out.println("O n�mero de l�mpadas � serem usadas no comodo mencionado s�o: " + lampadas);
		
	}

}
